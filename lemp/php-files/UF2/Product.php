<?php
class Product {
    //Properties
    private int $id;
    private string $name;
    private float $price;
    private array $color;


    //Constructor (new)
    function __construct(int $id, string $name, float $price, array $color) { //dos guions baixos sempre
        //accedir atributs
        $this ->id = $id; //argument amb el nom que volem --> (int $id) i s'assigna al objecte --> (private int $id)
        $this -> name = $name;
        $this -> price = $price;
        $this -> color = $color;    
    }

    //Destructor (opcional)
    function __destruct() {
        // unset($this); --> No es pot recuperar

    }

    //Getter and setter
    function __get_id(): int { 
        return $this -> id; // Despres de la flecha mai $ sempre la variable
    }
    function __set_id(int $id): void {
        $this -> id = $id;
    }


    function __get_name (): string {
        return $this -> name;
    }
    function __set_name(string $name): void {
        $this -> name = $name;
    }


    function __get_price(): float{
        return $this -> price;
    }
    function __set_price(float $price): void {
        $this -> price = $price;
    }


    function __get_color(): array {
        return $this -> color;
    }
    function __set_color(array $color): void {
        $this -> color = $color;
    }



    //Class Methods (opcional)
    function tax(float $tax): float {
        return $this->price * $tax/(1+ $tax);
    }
    function priceNoTax(float $tax): float {
        return $this->price/(1+$tax);
    }

    //toString (opcional)
    function __toString(): string {
        foreach ($this->colors as $color) {
            $text .=$color . " ";
        }
        return "Product[id=" . $this -> id .", name=". $this -> name .", price=". $this -> price .", colors=". $this -> color;
    }
}
$colors = ["Red", "Blue", "Green"];
$p1 = new Product(1, "Basic T-shirt", 12.55, $colors);

$p2 = new Product(2, "Long T-shirt", 18.75, ["Black"]);

echo $p1->__get_id() . "<br />";
echo $p1->__get_name() . "<br />" ;
echo $p1->__get_price() . "<br />";

$array_colors = $p1->__get_color();
print_r($array_colors);
echo "<br />";

var_dump($p1->__get_color());
foreach ($array_colors as $color) {
    echo $color ."<br />";
}

var_dump($array_colors);
echo "<br />";

foreach ($array_colors as $color) {
    echo $color ."";
}

$products2 = [];

$products2[] = $p1; //array_push ($products2, $p1)

$products2[] = $p2;

foreach ($products2 as $p) {
    echo $p->__get_id() . "<br />";
    echo $p->__get_name() . "<br />";
    echo $p->__get_price() . "<br />";

    $array_colors = $p->__get_color();
    print_r($array_colors);
    echo "<br />";
}
?>

<?php
echo "<br />";
echo "Setters: <br />";
$p1->__set_id(10);
echo $p1->__get_id() . "<br />";

$p1->__set_name("Basic T-shirt 2");
echo $p1->__get_name() . "<br />";

$p1->__set_price(15.75);
echo $p1->__get_price() . "<br />";

?>

<?php
echo "<br />";
echo "Methods: <br />";
echo $p1->priceNoTax(0,21) . "<br />";
echo $p1->tax(0.21);
?>