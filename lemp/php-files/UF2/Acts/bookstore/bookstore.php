<!DOCTYPE html>
<html>
<head>
    <title>Buscar libros</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 8px;
        }
    </style>
</head>
<body>

<h1>Buscar libros</h1>

<form method="GET">
    <label for="search">Buscar:</label>
    <input type="text" id="search" name="search" placeholder="ID, Título o ISBN">
    
    <label for="searchType">Buscar por:</label>
    <select id="searchType" name="searchType">
        <option value="id">ID</option>
        <option value="title">Título</option>
        <option value="isbn">ISBN</option>
    </select>
    
    <input type="submit" value="Buscar">
</form>

<?php
$servername = "172.22.0.2";
$username = "root";
$password = "1234";
$dbname = "bookstore";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Error de conexión a la base de datos: " . $conn->connect_error);
} else {
    echo "<p>Conexión a la base de datos exitosa</p>";
}

$search = isset($_GET['search']) ? $_GET['search'] : '';
$searchType = isset($_GET['searchType']) ? $_GET['searchType'] : 'title';

if (empty($search)) {
    $sql = "SELECT book_id, title, isbn13, publication_date FROM book ORDER BY book_id";
    $stmt = $conn->prepare($sql);
} else {
    if ($searchType === 'id') {
        $sql = "SELECT book_id, title, isbn13, publication_date FROM book WHERE book_id LIKE ? ORDER BY book_id";
        $stmt = $conn->prepare($sql);
        if ($stmt) {
            $searchParam = '%' . $search . '%';
            $stmt->bind_param("s", $searchParam);
        }
    } elseif ($searchType === 'title') {
        $sql = "SELECT book_id, title, isbn13, publication_date FROM book WHERE LOWER(title) LIKE LOWER(?) ORDER BY book_id";
        $stmt = $conn->prepare($sql);
        if ($stmt) {
            $searchParam = '%' . $search . '%';
            $stmt->bind_param("s", $searchParam);
        }
    } elseif ($searchType === 'isbn') {
        $sql = "SELECT book_id, title, isbn13, publication_date FROM book WHERE isbn13 LIKE ? ORDER BY book_id";
        $stmt = $conn->prepare($sql);
        if ($stmt) {
            $searchParam = '%' . $search . '%';
            $stmt->bind_param("s", $searchParam);
        }
    }
}

if ($stmt) {
    $stmt->execute();
    $result = $stmt->get_result();
    
    // Mostrar resultados
    if ($result->num_rows > 0) {
        echo "<table>";
        echo "<tr><th>ID</th><th>Título</th><th>ISBN</th><th>Fecha Publicación</th></tr>";
        while ($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["book_id"] . "</td>";
            echo "<td>" . $row["title"] . "</td>";
            echo "<td>" . $row["isbn13"] . "</td>";
            echo "<td>" . $row["publication_date"] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "<p>No se encontraron datos</p>";
    }
    $stmt->close();
}

$conn->close();
?>

</body>
</html>





