<?php

class Pokemons {
    
    // Propiedades o atributos
    private array $pokemons;

    // Constructor
    function __construct(){
        $this->pokemons = array();
    }

    // Getters and setters
    function get_pokemons(): array {
        return $this->pokemons;
    }

    function set_pokemons(array $pokemons): void {
        $this->pokemons = $pokemons;
    }

    // Method
    function __toString(): string {
        return "Pokemon:\n" .
        "Pokemons: " . $this->get_pokemons();
    }

    function add_pokemon(Pokemon $pokemon){
        $this->pokemons[] = $pokemon;
    }

    
    function get_generation(int $generation){
        $pokemons_generacio = array();

        foreach ($this->pokemons as $pokemon) {
            if ($pokemon->get_generation() === $generation) {
                $pokemons_generacio[] = $pokemon;
            }
        }
        return $pokemons_generacio;
    }


    function get_pokemon_name(string $text){
        $text = strtolower($text);
        $pokemons_name = array();
        
        foreach ($this->pokemons as $pokemon) {
            if (str_contains(strtolower($pokemon->get_name()), $text)) {
                $pokemons_name[] = $pokemon;
            }
        }
        return $pokemons_name;
    }
    
    

    function get_pokemon_type(string $text){
        $text = strtolower($text); 
        $pokemons_type = array();

        foreach ($this->pokemons as $pokemon) {
            if (strpos(strtolower($pokemon->get_type1()), $text) !== false || strpos(strtolower($pokemon->get_type2()), $text) !== false) {
                $pokemons_type[] = $pokemon;
            }
        }
        return $pokemons_type;
    }
}


?>
