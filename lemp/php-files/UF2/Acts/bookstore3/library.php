<?php
class Library
{
    // Properties
    private string $name;
    private array $books;

    // Constructor
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->books = [];
    }

    // Getters and setters
    public function add_book(Book $book): void
    {
        $this->books[] = $book;
    }

    public function get_books_by_title(string $title): array
    {
        $filtered_books = [];
        foreach ($this->books as $book) {
            if (stripos($book->getTitle(), $title) !== false) {
                $filtered_books[] = $book;
            }
        }
        return $filtered_books;
    }

    public function get_books_by_isbn13(string $isbn13): array
    {
        $filtered_books = [];
        foreach ($this->books as $book) {
            if ($book->getIsbn13() === $isbn13) {
                $filtered_books[] = $book;
            }
        }
        return $filtered_books;
    }

    public function get_books_by_author_name(string $author_name): array
    {
        $filtered_books = [];
        foreach ($this->books as $book) {
            foreach ($book->getAuthors() as $author) {
                if (stripos($author->getAuthorName(), $author_name) !== false) {
                    $filtered_books[] = $book;
                    break; // Break out of the inner loop once found
                }
            }
        }
        return $filtered_books;
    }

    // toString
    public function __toString(): string
    {
        $output = "Library Name: " . $this->name . "<br>";
        $output .= "Books:<br>";
        foreach ($this->books as $book) {
            $output .= $book->__toString() . "<br>";
        }
        return $output;
    }
}
?>

