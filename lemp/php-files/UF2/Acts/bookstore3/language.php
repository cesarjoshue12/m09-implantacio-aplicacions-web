<?php
class Language
{
    // Properties
    private int $language_id;
    private string $language_code;
    private string $language_name;

    // Constructor
    public function __construct(int $language_id, string $language_code, string $language_name)
    {
        $this->language_id = $language_id;
        $this->language_code = $language_code;
        $this->language_name = $language_name;
    }

    // Getters
    public function getLanguageId(): int
    {
        return $this->language_id;
    }

    public function getLanguageCode(): string
    {
        return $this->language_code;
    }

    public function getLanguageName(): string
    {
        return $this->language_name;
    }

    // Setters
    public function setLanguageId(int $language_id): void
    {
        $this->language_id = $language_id;
    }

    public function setLanguageCode(string $language_code): void
    {
        $this->language_code = $language_code;
    }

    public function setLanguageName(string $language_name): void
    {
        $this->language_name = $language_name;
    }

    // toString
    public function __toString(): string
    {
        return "Language ID: " . $this->language_id . "<br>" .
               "Language Code: " . $this->language_code . "<br>" .
               "Language Name: " . $this->language_name . "<br>";
    }
}
?>

