<?php

echo "<table class='table table-striped table-bordered'>";
    
    echo "<tr class='table-info'>";
        echo "<th>Angle</th>";
        echo "<th>Radian</th>";
        echo "<th>Sinus</th>";
        echo "<th>Cosinus</th>";
    
    echo "</tr>";
for ($angle = 0; $angle <= 360; $angle++) {
    $radians = round(deg2rad($angle),4 );
    $sinus = round(sin($radians), 4);
    $cosinus = round(cos($radians), 4);
    
    echo "<tr>";
    echo '<td class="text-primary">' . $angle . '</td>'; 
    echo '<td class="text-primary">' . $radians . '</td>';    

    if ($sinus < 0){
        echo '<td class="text-danger">' . $sinus . '</td>'; 
    } else {
        echo '<td class="text-primary">' . $sinus . '</td>';
    }
    if ($cosinus < 0){
        echo '<td class="text-danger">' . $cosinus . '</td>';
    } else {
        echo '<td class="text-primary">' . $cosinus . '</td>';
    }
    echo "</tr>";
}

echo "</table>";

?>