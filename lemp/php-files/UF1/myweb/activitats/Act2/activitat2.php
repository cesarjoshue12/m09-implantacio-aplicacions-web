<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Activitat 2</title>
</head>

<body>
    
    <div class="container mt-3">
        <h1>Sine and Cosine</h1>
        <div class="row text-center" >
            <div class="col-12 m-4">
                <img class="img-fluid" src="sine&cosine.png" width="600">
            </div>
        </div>
        
        <?php include "ex2.php" ?>
    </div>

</body>
</html>

