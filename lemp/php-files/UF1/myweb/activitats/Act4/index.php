<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activitat4</title>
    <link href="style.css" type="text/css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>
    <div class="container mt-3">
        <h1>PRICE, TAX and ROUNDS</h1>
        <form method="post">
            <label for="preu_iva">Preu amb IVA:</label>
            <input type="number" step="0.01" name="preu_iva" required>
            <br>
            <label for="iva">Tant per cent d'IVA:</label>
            <input type="number" step="0.01" name="iva" required>
            <br>
            <input type="submit" name="Calcular" value="Calcular">
        </form>
    </div>
    <?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["preu_iva"]) && isset($_POST["iva"])) {
            $iva = $_POST["iva"];
            // Càlcul amb tots els decimals
            $preu_sense_iva_exacte = $preu_iva / (1 + $iva / 100);
            // Utilitza la funció round() per mostrar el càlcul amb 2 decimals
            $preu_sense_iva_arrodonit = round($preu_sense_iva_exacte, 2);
            // Utilitza la funció sprintf() per mostrar el càlcul amb 4 decimals
            $preu_sense_iva_format = sprintf("%.4f", $preu_sense_iva_exacte);
            
            echo "<h2>Resultats:</h2>";
            echo "<p>Càlcul amb tots els decimals: $preu_sense_iva_exacte</p>";
            echo "<p>Utilitza la funció round() per mostrar el càlcul amb 2 decimals: $preu_sense_iva_arrodonit</p>";
            echo "<p>Utilitza la funció sprintf() per mostrar el càlcul amb 4 decimals: $preu_sense_iva_format</p>";
        } else {
            echo "Por favor, completa todos los campos.";
        }
    }
    ?>

</body>

</html>