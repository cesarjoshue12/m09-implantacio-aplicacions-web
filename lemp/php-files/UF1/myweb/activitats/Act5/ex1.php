<!DOCTYPE html>
<html>
<head>
    <title>Factorial Calculator</title>
</head>
<body>
    <h1>Factorial Calculator</h1>
    <p>Enter a number to calculate its factorial:</p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <input type="number" name="number" value="<?php echo isset($_POST['number']) ? $_POST['number'] : ''; ?>" required><br/><br/>
        <button type="submit">Calculate Factorial</button>
    </form>
    <?php
    function factorial($n) {
        if ($n == 0) {
            return 1;
        } else {
            return $n * factorial($n - 1);
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $number = $_POST['number'];
        echo "<h2>Factorial of $number:</h2>";
        echo "<p>" . $number."! " . "= " . factorial($number) . "</p>";
    }
    ?>

    <h2>Factorial Formulas</h2>
    <p>
        The formula to calculate a factorial for a number is: <br/>
        n! = n × (n-1) × (n-2) × ... × 1 <br/>
        Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the number 1 is reached. <br/>
        The factorial of zero is 1: <br/>
        0! = 1 <br/>
   </p>

    <h2>Recurrence Relation</h2>
    <p>
        And the formula expressed using the recurrence relation looks like this: <br/>
        n! = n × (n – 1)! <br/>
        So the factorial n! is equal to the number n times the factorial of n minus one. This recurses until n is equal to 0. <br/>
    </p>

    <table border="1">
        <tr>
            <th>Number</th>
            <th>Factorial</th>
        </tr>
    <?php
        
        for ($i = 0; $i <= 100; $i++) {
            echo "<tr>";
            echo "<td>" . $i . "</td>"; 
            echo "<td>" . factorial($i) ."</td>"; 
            echo "</tr>";
        }
    ?>
    </table>
</body>
</html>

