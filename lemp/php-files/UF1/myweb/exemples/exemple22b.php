<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
    <head>
        <title>Document</title>
    </head>
    <body>
    <?php
   
    $h = date("H");
    $y = date("Y");
    $d = date("Y/m/d");
    echo $h . "<br>";
    echo $y . "<br>";
    echo $d . "<br>";

    if ($h < "10") {
        echo "Have a good morning!";
    } elseif ($h < "20") {
        echo "Have a good day!";
    } else {
        echo "Have a good night!";
    }

    echo "<br />";

    $favcolor = "red";

    switch ($favcolor) {
        case "red":
            echo "Your favorite color is red!";
            break;
        case "blue":
            echo "Your favorite color is blue!";
            break;
        case "green":
            echo "Your favorite color is green!";
            break;
        default:
            echo "Your favorite color is neither red, blue, nor green!";
    }

    echo "<br /> Loops <br />";

    for ($x = 0; $x < 10; $x++) {
        if ($x == 4) {
            break;
        }
        echo "The number is: $x <br>";
    }
    ?>


    </body>
</html>