<?php
    $array1 = array("RED", "BLUE", "GREEN");

    $array2 = ["CYAN", "YELLOW", "MAGENTA", "BLACK"];

    echo $array1[0] . "<br />";
    echo $array1[1] . "<br />";
    echo $array1[2] . "<br />";

    $len = count($array1);

    // For amb un quadrat
    // Count($array1) per fer un bucle per tots el elements de la llista
    for ($i=0; $i < count($array1); $i++) { 
        # code...
        echo $array1[$i] . " ";
    }

    // Darles valores a cada item
    //clau -> valor
    $games = array(
        "Playstation 2" => 155,
        "Game Boy" => "GREEN",
        "Wii" => "BLUE",
    );

    // Seleccionar item
    echo "<br />";
    echo $games["Playstation 2"] . "<br />";

    // Foreach hace un select de todos y muestra array game en este caso(clau -> valor)
    foreach ($games as $key => $value) {
        # code...
        echo $key . ": " . $value . "<br />";
    }

    //multi array (fila columna)
    $cars = array (
        array("Volvo",22,18),
        array("BMW",15,13),
        array("Saab",5,2),
        array("Land Rover",17,15)
    );

    //mostrar fila y columna
    echo $cars[0][0] . ": In stock: " . $cars[0][1] . ", sold: " . $cars[0][2] . ".<br>";
    echo $cars[1][0] . ": In stock: " . $cars[1][1] . ", sold: " . $cars[1][2] . ".<br>";
    echo $cars[2][0] . ": In stock: " . $cars[2][1] . ", sold: " . $cars[2][2] . ".<br>";
    echo $cars[3][0] . ": In stock: " . $cars[3][1] . ", sold: " . $cars[3][2] . ".<br>";
    
    //iterar array
    //sobra un ul per cada row y s'afageix li per cada cosa que hi ha dins
    for ($row = 0; $row < 4; $row++) {
        echo "<p><b>Row number $row</b></p>";
        echo "<ul>";
        for ($col = 0; $col < 3; $col++) {
            echo "<li>" . $cars[$row][$col] . "</li>";
        }
        echo "</ul>";
    }
    
?>