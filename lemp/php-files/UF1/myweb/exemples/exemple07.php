<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
    <head>
        <title>Document</title>
    </head>
    <body>
        <?php
            $h1 = "Title Page";
            $txt = "<h1>$h1</h1>";
            $txt .= "<h2>Header h2</h2>";
            echo $txt;

            $p = "This is a very silly line with a  \$txt";
            echo "<p>$p</p>";

            function myFunction1() {
                global $div;
                $div = "This is a div";
                echo "<div>$div</div>";
                // echo "<div>$txt</div>;
            }

            myFunction1();
            echo $div;

            echo "<br />GLOBAL:<br />";
            echo $GLOBALS['h1'];
            echo $GLOBALS['txt'];
            echo $GLOBALS['div'];

            function myfunction2() {

            }

            echo "<br />";

            if (isset($h1)) {
                echo "\$h1 des not exist" . "<br />";
            }

            $y = 100;
            $f = 20.56;
            $t = "Text";
            $b = true;

            $result = sprintf("This is the result: %d, %.3f, %s, %b", $y , $f, $t, $b);

            echo "<br />" . $result . "<br />";

            echo "{$y} {$b}" . "<br />";

            $lorem = "En la industria editorial i en disseny grafic, lorem ipsum es un text de farciment que susa habitualment per a mostrar els elements grafics dun document, com ara la tipografia o la composicio";

            echo strlen($lorem) . "<br />";
            echo str_word_count($lorem) . "<br />";
            echo strpos($lorem, "dummy", 30) . "<br />";

            $z = 120;
            echo PHP_INT_MAX . "<br />";
            echo $z * 2.89 . "<br />"


        ?>
    </body>
</html>
