<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>Forms</title>
</head>

<body>
    <?php
        function validate($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        if ($_POST) {
            if(isset($_POST["execute"])){
                execute();
            }
        }

        function execute() {
            if (isset($_POST["name"]))
                echo "Name" .  validate($_POST['name']) . "<br />";
            if (isset($_POST["email"]))
                echo "Email" .  validate($_POST['email']) . "<br />";
        }
        
    ?>
</body>
</html>