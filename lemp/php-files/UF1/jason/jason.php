<?php

// Json OBJECT
$age = array("Peter" => 35, "Ben" => 37, "Joe" => 43);

$json = json_encode($age);

var_dump($json);

$jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';

$array2 = json_decode($jsonobj);

var_dump($array2);

echo "<br />";
echo $array2 -> Peter . "<br />";
echo $array2 -> Ben . "<br />";
echo $array2 ->Joe . "<br />";

$array3 = json_decode($jsonobj,true);

var_dump($array3);

echo "<br />";
echo $array3["Peter"] . "<br />";
echo $array3["Ben"] . "<br />";
echo $array3["Joe"] . "<br />";

$url = "https://jsonplaceholder.typicode.com/posts/1";


//JSON ARRAY


?>
